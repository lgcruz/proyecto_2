#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include "libreria.h"
#include <pthread.h>

Indice* crearIndice(int id){
	Indice *ind=malloc(sizeof(Indice*));
	ind->id = id;
	ind->hashtableTDA = crear_hash_table(10);
	return ind;
}

void generarIndice(Indice* ind, char* rutaArchivo){
	
	int indice = 0;
	//int vueltas = 0;
	printf("nombre %s\n",rutaArchivo);
	
	//eliminar el \n en fgets
	char *correccion;
	if((correccion=strchr(rutaArchivo,'\n'))!=NULL){
		*correccion='\0';
	}
	indice+=strlen(rutaArchivo);
	char nombre_file[20];
	
	strcpy(nombre_file,"./data/");
	
	strcat(nombre_file,rutaArchivo);
	
	strcat(nombre_file,".txt");
	printf("%s\n",nombre_file);
	
	int status;
	pthread_t hilo;
	args* a=malloc(sizeof(args*));
	a->nombre_file = nombre_file;
	a->ind = ind;
	status = pthread_create(&hilo,NULL,AnalizadorTexto,(void *)a);
	if(status<0){
		fprintf(stderr,"Error al crear el hilo \n");
		exit(-1);
	}
	for(int i =0;i<10;i++){
		Objeto *pal = &ind->hashtableTDA->buckets[i];
		if(pal->palabra==NULL){
			printf("Bucket %d vacio\n",i);
		}else{
			printf("Bucket %d tiene %s ",i,pal->palabra);	
			while(pal->siguiente!=NULL){
				Posicion *p1=pal->ubicacion->posicionTDA;
				printf("[%d",p1->posicion);
				while(p1->siguiente!=NULL){
					p1 = p1->siguiente;
					printf(", %d",p1->posicion);
				}printf("]");
				pal = pal->siguiente;
				printf(", %s",pal->palabra);
			}printf("\n");
		}
	}
}

void* AnalizadorTexto(void* arg){
	int errnum;
	FILE *file;
	args* a = (args*)arg;
	file = fopen(a->nombre_file,"r");
		
	if(file!=NULL){
		int indice=0;
		char *buff=(char *)malloc(sizeof(char)*50);
		while(fscanf(file,"%s",buff) != EOF){
		
			//printf("%d ",(int)strlen(buff));
			for(int i=0;i<strlen(buff);i++){
				if(isupper(buff[i])){
					buff[i]=buff[i]+32;
				}
			}char* letra = strchr(buff,',');
			while(letra){
				*letra=*(letra+1);
				
				letra= strchr(letra+1,',');
			}letra = strchr(buff,'.');
			while(letra){
				*letra=*(letra+1);
				
				letra= strchr(letra+1,'.');
			}letra = strchr(buff,'?');
			while(letra){
				*letra= *(letra+1);
				
				letra= strchr(letra+1,'?');
			}letra = strchr(buff,'!');
			while(letra){
				*letra= *(letra+1);
				
				letra= strchr(letra+1,'!');
			}
			//printf(" %s \n",buff);
			if(obtener(a->ind->hashtableTDA,buff)==NULL){
				Posicion *p = malloc(sizeof(Posicion *)+sizeof(int));
				p->posicion = indice;
				p->siguiente = NULL; 
				Ubicacion *u = malloc(sizeof(Posicion *)+sizeof(Ubicacion *)+sizeof(char *));
				u->archivo=a->nombre_file;
				u->posicionTDA =p;
				u->siguiente = NULL;
				Objeto *o = malloc(sizeof(Ubicacion *)+sizeof(Objeto *)+sizeof(char *));
				o->palabra=buff;
				o->ubicacion=u;
				o->siguiente=NULL;
			
				insertar(a->ind->hashtableTDA,buff,o);
			
			}
			else{
				//printf("(Ya existe esa palabra)\n");
				Posicion *p = malloc(sizeof(Posicion *)+sizeof(int));
				p->posicion = indice;
				p->siguiente = NULL; 

				Ubicacion *u = malloc(sizeof(Posicion *)+sizeof(Ubicacion *)+sizeof(char *));
				u->archivo=a->nombre_file;
				u->posicionTDA =p;
				u->siguiente = NULL;
				Objeto *o = malloc(sizeof(Ubicacion *)+sizeof(Objeto *)+sizeof(char *));
				o->palabra=buff;
				o->ubicacion=u;
				o->siguiente=NULL;
				insertar(a->ind->hashtableTDA,buff,o);
				//printf("%d \n",o->ubicacion->posicionTDA->posicion);
			
			}
			indice+=1;
			//printf("%d %d\n",(int)strlen(buff),indice);
			buff=(char *)malloc(sizeof(char)*50);
			
		}	
		printf("\n");
		fclose(file);
	}else{
		errnum = errno;
		fprintf(stderr," valor de errno: %d\n",errno);
		perror("Error mostrado por perror");
		fprintf(stderr,"Error al abrir el archivo: %s\n",strerror(errnum));
	}return 0;
}

Respuesta* consultarPalabra(Indice* ind, char* palabra){
	Respuesta *rspst=malloc(sizeof(Respuesta*));
	int bucket = ind->hashtableTDA->cantidad;
	for(int i =0;i<bucket;i++){
		Objeto *pal = &ind->hashtableTDA->buckets[i];
		if(pal->palabra!=NULL){
			while((pal->siguiente!=NULL) && (strcmp(pal->palabra,palabra)!=0)){
				pal = pal->siguiente;
			}if(strcmp(pal->palabra,palabra)==0){
				rspst->cabecera_lista_palabras = pal;
				return rspst;
			}
		}
	}
	return NULL;
}

//Respuesta* consultarPalabras(Indice* ind, char** palabras){

//}
