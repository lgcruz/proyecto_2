#include "libreria.h"
#include <stdio.h>
#include <stdlib.h>

#include <string.h>

unsigned long hash(unsigned char *str){
	unsigned long hash = 5381;
	int c;

	while ((c = *str++)){
		hash = ((hash << 5) + hash) + c;
	}
	return hash;
}

HashTable *crear_hash_table(int bucket){
	HashTable *tabla = malloc(sizeof(int)+sizeof(Objeto)*bucket);
	tabla->numeroBuckets = bucket;
	tabla->cantidad = 0;
	return tabla;
}

void insertar(HashTable *table, char* clave, Objeto* objeto){
	long clave_h = hash((unsigned char*)clave);
	int pos = clave_h % table->numeroBuckets;
	if (table->buckets[pos].palabra==NULL){
		table->buckets[pos]=*objeto;
		table->cantidad++;
	}else{
		Objeto *obj = &table->buckets[pos];
		while((obj->siguiente!=NULL) && (strcmp(obj->palabra,clave)!=0)){
			obj = obj->siguiente;
		}
		if((strcmp(obj->palabra,clave)==0)){
			Ubicacion *ubi = obj->ubicacion;
			while((ubi->siguiente!=NULL) && (strcmp(ubi->archivo,objeto->ubicacion->archivo)!=0)){
				ubi=ubi->siguiente;
			}
			if(strcmp(ubi->archivo,objeto->ubicacion->archivo)==0){
				Posicion *posi = ubi->posicionTDA;
				while((posi->siguiente!=NULL) && (posi->posicion!=objeto->ubicacion->posicionTDA->posicion)){
					posi=posi->siguiente;
				}
				if(posi->posicion!=objeto->ubicacion->posicionTDA->posicion){
					posi->siguiente = objeto->ubicacion->posicionTDA;	
					//printf("agrega %d \n",objeto->ubicacion->posicionTDA->posicion);
				}
			}else{
				ubi->siguiente=objeto->ubicacion;
			}
		}else{
			obj->siguiente=objeto;
			table->cantidad++;
		}
	}
}

Objeto* obtener(HashTable *table, char* clave){
	long clave_hash = hash((unsigned char*)clave);
	int pos = clave_hash % table->numeroBuckets;
	//printf("posi %d %s\n",pos,clave);
	if(table->buckets[pos].palabra==NULL){
		//printf("No existe %s\n",clave);
		return NULL;
	}else{		
		
		Objeto *obj = &table->buckets[pos];
		while((obj->siguiente!=NULL) && (strcmp(obj->palabra,clave)!=0)){
			obj=obj->siguiente;
		}if(strcmp(obj->palabra,clave)==0){
			//printf("Existe %s %s\n",clave,obj->palabra);
			return obj;
		}//printf("No existe %s\n",clave);
		return NULL;
	}
	
}
