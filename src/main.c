#include <stdio.h>
#include <stdlib.h>
#include "libreria.h"
#include <ctype.h>
#include <string.h>
#include <errno.h>

int main(){
	int bucket;
	int errnum;
	printf("Ingrese la cantidad de buckets para el hashTable\n");
	scanf("%d",&bucket);
	HashTable *tabla = crear_hash_table(bucket);
			
	FILE *file;
	char nombre[20];
	
	int vueltas = 0;
	int indice = 0;
	while(strcmp(nombre,"no")!=0){
		printf("Igrese el nombre del archivo.txt (salir = no)\n");
		if(vueltas == 0){
			fgets(nombre,50,stdin);
		}vueltas+=1;
		fgets(nombre,50,stdin);
		
		printf("nombre %s\n",nombre);
		
		//eliminar el \n en fgets
		char *correccion;
		if((correccion=strchr(nombre,'\n'))!=NULL){
			*correccion='\0';
		}

		indice+=strlen(nombre);
		char nombre_file[9+(int)strlen(nombre)];
		strcat(nombre_file,"data/");
		strcat(nombre_file,nombre);
		strcat(nombre_file,".txt");
		printf("%s\n",nombre_file);
		file = fopen(nombre_file,"r");
		
		if(file!=NULL){
			int indice=0;
			char *buff=(char *)malloc(sizeof(char)*50);
			while(fscanf(file,"%s",buff) != EOF){
			
				//printf("%d ",(int)strlen(buff));
				for(int i=0;i<strlen(buff);i++){
					if(isupper(buff[i])){
						buff[i]=buff[i]+32;
					}
				}

				char* letra = strchr(buff,',');
				while(letra){
					*letra=*(letra+1);
					indice+=1;
					letra= strchr(letra+1,',');
				}letra = strchr(buff,'.');
				while(letra){
					*letra=*(letra+1);
					indice+=1;
					letra= strchr(letra+1,'.');
				}letra = strchr(buff,'?');
				while(letra){
					*letra= *(letra+1);
					indice+=1;
					letra= strchr(letra+1,'?');
				}letra = strchr(buff,'!');
				while(letra){
					*letra= *(letra+1);
					indice+=1;
					letra= strchr(letra+1,'!');
				}
				//printf(" %s \n",buff);
				if(obtener(tabla,buff)==NULL){
					Posicion *p = malloc(sizeof(Posicion *)+sizeof(int));
					p->posicion = indice;
					p->siguiente = NULL; 

					Ubicacion *u = malloc(sizeof(Posicion *)+sizeof(Ubicacion *)+sizeof(char *));
					u->archivo=nombre_file;
					u->posicionTDA =p;
					u->siguiente = NULL;

					Objeto *o = malloc(sizeof(Ubicacion *)+sizeof(Objeto *)+sizeof(char *));
					o->palabra=buff;
					o->ubicacion=u;
					o->siguiente=NULL;
				
					insertar(tabla,buff,o);
				
				}
				else{
					//printf("(Ya existe esa palabra)\n");

					Posicion *p = malloc(sizeof(Posicion *)+sizeof(int));
					p->posicion = indice;
					p->siguiente = NULL; 

					Ubicacion *u = malloc(sizeof(Posicion *)+sizeof(Ubicacion *)+sizeof(char *));
					u->archivo=nombre_file;
					u->posicionTDA =p;
					u->siguiente = NULL;

					Objeto *o = malloc(sizeof(Ubicacion *)+sizeof(Objeto *)+sizeof(char *));
					o->palabra=buff;
					o->ubicacion=u;
					o->siguiente=NULL;
					insertar(tabla,buff,o);
					//printf("%d \n",o->ubicacion->posicionTDA->posicion);
				
				}
				indice+=strlen(buff)+1;
				//printf("%d %d\n",(int)strlen(buff),indice);
				buff=(char *)malloc(sizeof(char)*50);
			
			}	
			printf("\n");
			fclose(file);
		}else{
			errnum = errno;
			fprintf(stderr," valor de errno: %d\n",errno);
			perror("Error mostrado por perror");
			fprintf(stderr,"Error al abrir el archivo: %s\n",strerror(errnum));
		}
		for(int i =0;i<bucket;i++){
			Objeto *pal = &tabla->buckets[i];
			if(pal->palabra==NULL){
				printf("Bucket %d vacio\n",i);
			}else{
				printf("Bucket %d tiene %s",i,pal->palabra);	
				while(pal->siguiente!=NULL){
					Posicion *p1=pal->ubicacion->posicionTDA;
					printf("[%d",p1->posicion);
					while(p1->siguiente!=NULL){
						p1 = p1->siguiente;
						printf(", %d",p1->posicion);
					}printf("]");
					pal = pal->siguiente;
					printf(", %s",pal->palabra);
				}printf("\n");
			}
		}
	}
	return 0;
}
