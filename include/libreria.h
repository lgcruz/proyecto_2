#ifndef HEADER_FILE__
#define HEADER_FILE__

typedef struct posicionTDA Posicion;
typedef struct ubicacionTDA Ubicacion;
typedef struct objetoTDA Objeto;
typedef struct hashtableTDA HashTable;
typedef struct indice Indice;
typedef struct respuesta Respuesta;
typedef struct argumentos args;
struct posicionTDA{
	int posicion;
	Posicion *siguiente;
};

struct ubicacionTDA{
	char *archivo;
	Posicion *posicionTDA;
	Ubicacion *siguiente;
};

struct objetoTDA{
	char *palabra;
	Ubicacion *ubicacion;
	Objeto *siguiente;
};
struct hashtableTDA{
	int cantidad;
	int numeroBuckets;
	Objeto buckets[];
};
struct indice{
	int id;
	HashTable* hashtableTDA;
};
struct respuesta{
	Objeto* cabecera_lista_palabras;
};
struct argumentos{
	char* nombre_file;
	Indice* ind;
};
Indice* crearIndice(int id);
void* AnalizadorTexto(void* arg);
void generarIndice(Indice* ind, char* rutaArchivo);
Respuesta* consultarPalabra(Indice* ind, char* palabra);
//Respuesta* consultarPalabras(Indice* ind, char** palabras);
unsigned long hash(unsigned char *str);
HashTable *crear_hash_table(int buckets);
void insertar(HashTable *table, char* clave, Objeto* objeto);
Objeto* obtener(HashTable *table, char* clave);

#endif
